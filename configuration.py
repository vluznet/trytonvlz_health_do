# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Configuration(metaclass=PoolMeta):
    __name__ = 'health.configuration'
    product_copayment = fields.Many2One('product.product', 'Producto Copago',
        domain=[('type', '=', 'service')])
    invoice_service_method = fields.Selection([
        ('split_copayment', 'Split by Copayment'),
        ('unique', 'Unique'),
        ('', '')
        ], 'Invoice Services Method')
    product_ars = fields.Many2One('product.product', 'Servicio ARS',
        domain=[('type', '=', 'service')])
