# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
# import datetime
from datetime import datetime
from datetime import date
import calendar
from trytond.model import fields, ModelView
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateAction, StateReport, Button
from trytond.report import Report


class PrintRipsStart(ModelView):
    'Print Rips Report Start'
    __name__ = 'health_do.print_rips.start'
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)
    report = fields.Many2One('ir.action.report', 'Report', domain=[
            ('module', '=', 'health_do'),
            ('report_name', 'like', '%rip%'),
        ], required=True)
    institution = fields.Many2One('health.institution', 'Institution',
            required=True)

    @fields.depends('date_start', 'date_end')
    def on_change_date_start(self, name=None):
        if self.date_start:
            w, number_days = calendar.monthrange(self.date_start.year, self.date_start.month)
            self.date_end = date(self.date_start.year, self.date_start.month, number_days)

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()


class PrintRips(Wizard):
    'Print Rips'
    __name__ = 'health_do.print_rips'
    start = StateView('health_do.print_rips.start',
        'health_do.print_rips_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateAction('health_do.report_rips')

    def do_print_(self, action):
        data = {
            'date_start': self.start.date_start,
            'date_end': self.start.date_end,
            'institution': self.start.institution.id,
        }
        action['report'] = self.start.report.report
        action['report_name'] = self.start.report.report_name
        action['id'] = self.start.report.id
        action['action'] = self.start.report.action.id
        return action, data

    def transition_print_(self):
        return 'end'


class RipUsuarios(Report):
    'Rip Usuarios'
    __name__ = 'health_do.rip.usuarios'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(RipUsuarios, cls).get_context(records, data)
        pool = Pool()
        Appointment = pool.get('health.appointment')
        # Invoice = pool.get('account.invoice')
        # invoices = Invoice.search([
        #     ('invoice_date', '>=', data['date_start']),
        #     ('invoice_date', '<=', data['date_end']),
        #     ('type', '=', 'out'),
        #     ('state', 'in', ['posted', 'paid', 'validated']),
        #     ('patient', '!=', None),
        #     ('patient.name.eps', '!=', None),
        # ])

        appointments = Appointment.search([
            ('appointment_date', '>=', datetime.combine(data['date_start'], datetime.min.time())),
            ('appointment_date', '<=', datetime.combine(data['date_end'], datetime.max.time())),
            ('institution', '=', data['institution']),
        ])
        appointment_lines = []
        lines = []
        for appointment in appointments:
            if appointment.id in lines:
                continue
            appointment_lines.append(appointment)
            lines.append(appointment.id)
        report_context['institution'] = appointment_lines[0].institution.name.name
        report_context['records'] = appointment_lines
        return report_context


class RipTransacciones(Report):
    'Rip Transacciones AF'
    __name__ = 'health_do.rip.transacciones'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(RipTransacciones, cls).get_context(records, data)
        pool = Pool()

        Invoice = pool.get('account.invoice')
        Institution = pool.get('health.institution')

        records = Invoice.search([
            ('invoice_date', '>=', data['date_start']),
            ('invoice_date', '<=', data['date_end']),
            ('state', 'in', ['posted', 'paid', 'validated']),
            ('type', '=', 'out'),
        ])
        report_context['records'] = records
        report_context['date_start'] = data['date_start']
        report_context['date_end'] = data['date_end']
        report_context['institution'] = Institution(data['institution'])
        return report_context


class RipConsulta(Report):
    'Rip Consulta'
    __name__ = 'health_do.rip.consulta'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(RipConsulta, cls).get_context(records, data)
        pool = Pool()
        Invoice = pool.get('account.invoice')

        records = Invoice.search([
            ('invoice_date', '>=', data['date_start']),
            ('invoice_date', '<=', data['date_end']),
            ('type', '=', 'out'),
            ('state', 'in', ['posted', 'paid', 'validated']),
        ])
        Institution = Pool().get('health.institution')
        report_context['records'] = records
        report_context['date_start'] = data['date_start']
        report_context['date_end'] = data['date_end']
        report_context['institution'] = Institution(data['institution'])
        return report_context


class RipOtrosServicios(Report):
    'Rip Otros Servicios'
    __name__ = 'health_do.rip.otros_servicios'


class RipMedicamentos(Report):
    'Rip Medicamentos'
    __name__ = 'health_do.rip.medicamentos'


class RipUrgencias(Report):
    'Rip Urgencias'
    __name__ = 'health_do.rip.urgencias'


class RipRecienNacidos(Report):
    'Rip Recien Nacidos'
    __name__ = 'health_do.rip.recien_nacidos'


class RipHospitalizacion(Report):
    'Rip Hospitalizacion'
    __name__ = 'health_do.rip.hopitalizacion'


class GeneralEvaluationPatientStart(ModelView):
    'Print General Evaluation Patient Start'
    __name__ = 'health_do.general_Evaluation_Patient.start'
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)
    institution = fields.Many2One('health.institution', 'Institution',
            required=True)

    @fields.depends('date_start', 'date_end')
    def on_change_date_start(self, name=None):
        if self.date_start:
            w, number_days = calendar.monthrange(self.date_start.year, self.date_start.month)
            self.date_end = date(self.date_start.year, self.date_start.month, number_days)

    @staticmethod
    def default_institution():
        HealthInst = Pool().get('health.institution')
        institution = HealthInst.get_institution()
        return institution


class GeneralEvaluationPatient(Wizard):
    'Print General Evaluation Patient'
    __name__ = 'health_do.general_Evaluation_Patient'
    start = StateView('health_do.general_Evaluation_Patient.start',
        'health_do.print_general_Evaluation_Patient_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('health_do.general_Evaluation_Patient_Report')

    def do_print_(self, action):
        data = {
            'date_start': self.start.date_start,
            'date_end': self.start.date_end,
            'institution': self.start.institution.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class GeneralEvaluationPatientReport(Report):
    'General Evaluation Patient Report'
    __name__ = 'health_do.general_Evaluation_Patient_Report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(GeneralEvaluationPatientReport, cls).get_context(records, data)
        pool = Pool()
        PatientEvaluation = pool.get('health.patient.evaluation')

        records = PatientEvaluation.search([
            ('patient', '!=', None),
            ('appointment.appointment_date', '>=', datetime.combine(data['date_start'], datetime.min.time())),
            ('appointment.appointment_date', '<=', datetime.combine(data['date_end'], datetime.max.time())),
        ])
        Institution = Pool().get('health.institution')
        report_context['records'] = records
        report_context['date_start'] = data['date_start']
        report_context['date_end'] = data['date_end']
        report_context['institution'] = Institution(data['institution'])
        return report_context


class ProductInvoiceStart(ModelView):
    'Product Invoice Start'
    __name__ = 'health_do.product_invoice.start'
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)
    company = fields.Many2One('company.company', 'Company',
            required=True)
    product = fields.Many2One('product.product', 'Product', domain=[
            ('type', '=', 'service'),
            ('template.salable', '=', True),
    ], required=True)

    @fields.depends('date_start', 'date_end')
    def on_change_date_start(self, name=None):
        if self.date_start:
            w, number_days = calendar.monthrange(self.date_start.year, self.date_start.month)
            self.date_end = date(self.date_start.year, self.date_start.month, number_days)


class ProductInvoice(Wizard):
    'Product Invoice'
    __name__ = 'health_do.product_invoice'
    start = StateView('health_do.product_invoice.start',
        'health_do.print_product_invoice_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('health_do.product_invoice_report')

    def do_print_(self, action):
        data = {
            'date_start': self.start.date_start,
            'date_end': self.start.date_end,
            'company': self.start.company.id,
            'product': self.start.product.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ProductInvoiceReport(Report):
    'Product Invoice Report'
    __name__ = 'health_do.product_invoice_report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ProductInvoiceReport, cls).get_context(records, data)
        pool = Pool()
        LineInvoice = pool.get('account.invoice.line')

        lines = LineInvoice.search([
            ('invoice.company', '=', data['company']),
            ('invoice.invoice_date', '>=', data['date_start']),
            ('invoice.invoice_date', '<=', data['date_end']),
            ('invoice.type', '=', 'out'),
            ('invoice', '!=', None),
            ('invoice.state', 'in', ['posted', 'paid', 'validated']),
            ('product', '=', data['product'])
        ])
        Company = Pool().get('company.company')
        Product = Pool().get('product.product')
        report_context['records'] = lines
        report_context['product'] = Product(data['product'])
        report_context['date_start'] = data['date_start']
        report_context['date_end'] = data['date_end']
        report_context['company'] = Company(data['company'])
        return report_context
