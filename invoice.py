# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction

STATES = {
    'readonly': Eval('state') != 'draft',
}
_ZERO = Decimal('0')


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    amount_copayment = fields.Numeric('Amount Copayment', digits=(16, 2),
        states={'readonly': True}, depends=['lines'])
    net_amount = fields.Function(fields.Numeric('Net Amount', digits=(16, 2)),
        'get_net_amount')

    # @fields.depends('lines', 'amount_copayment')
    # def on_change_lines(self, name=None):
    #     self.amount_copayment = sum([l.copayment for l in self.lines])

    def get_net_amount(self, name):
        res = self.total_amount
        if self.amount_copayment:
            res = res - self.amount_copayment
        return res


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'
    STATES = {'readonly': Eval('state') != 'draft'}

    service_start = fields.Date('Service Start', states=STATES)
    service_end = fields.Date('Service End', states=STATES)
    authorization = fields.Char('Authorization', states=STATES)
    insurance_plan = fields.Many2One('health.insurance.plan',
        'Insurance Plan', states=STATES)
    copayment = fields.Numeric('Copayment', digits=(16, 2), states=STATES)
    patient = fields.Many2One('health.patient', 'Patient', states=STATES)
    area = fields.Char('Area', states=STATES)
    amount_service = fields.Numeric('Amount Service', digits=(16, 2),
        states={'readonly': True})


class InvoiceHealthReport(Report):
    __name__ = 'account.invoice_health'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(InvoiceHealthReport, cls).get_context(
            records, data)
        Company = Pool().get('company.company')
        report_context['company'] = Company(Transaction().context['company'])
        return report_context


class CreateServiceInvoice(metaclass=PoolMeta):
    __name__ = 'health.service.invoice.create'

    def get_invoice_data(self, party, service):
        res = super(CreateServiceInvoice, self).get_invoice_data(party, service)
        res['amount_copayment'] = service.copayment
        return res

    def get_invoice_line(self, line, unit_price, seq):
        InvoiceLine = Pool().get('account.invoice.line')
        print('Aqui...............', line)
        res = super(CreateServiceInvoice, self).get_invoice_line(
            line, unit_price, seq)
        if hasattr(InvoiceLine, 'gross_unit_price'):
            res['gross_unit_price'] = unit_price
        # if hasattr(InvoiceLine, 'gross_unit_price'):
        #     res['gross_unit_price'] = unit_price
        return res
