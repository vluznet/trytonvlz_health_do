
from datetime import date
from decimal import Decimal
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateTransition, StateView, Button

from trytond.i18n import gettext
from trytond.exceptions import UserError

ZERO = Decimal('0')


class HealthService(metaclass=PoolMeta):
    __name__ = 'health.service'
    STATES = {'readonly': Eval('state') != 'draft'}
    copayment = fields.Function(fields.Numeric('Copayment', digits=(16, 2),
        depends=['lines']), 'get_copayment')
    copayment_voucher = fields.Many2One('account.voucher', 'Copayment Voucher',
        states={'readonly': True})
    authorized_amount = fields.Function(fields.Numeric('Authorized Amount',
        digits=(16, 2), depends=['lines']), 'get_authorized_amount')
    customer_balance = fields.Function(fields.Numeric('Customer Balance',
        digits=(16, 2), depends=['lines']), 'get_customer_balance')
    customer_status = fields.Selection([
        ('paid', 'Paid'),
        ('pending', 'Pending'),
        ('', ''),
        ], 'Customer Status', readonly=True)

    @classmethod
    def __setup__(cls):
        super(HealthService, cls).__setup__()
        cls._buttons.update({
            'wizard_payment_form': {
                'invisible': Eval('customer_status') == 'paid',
            },
        })

    @classmethod
    def _get_origin(cls):
        origins = super(HealthService, cls)._get_origin()
        origins.append('health.emergency')
        return origins

    @staticmethod
    def default_customer_status():
        return ''

    def get_copayment(self, name=None):
        res = ZERO
        for l in self.lines:
            if l.copayment and l.copayment > 0 and l.qty:
                res += (l.copayment * l.qty)
        return res

    def get_customer_balance(self, name):
        res = []
        if self.total_amount and self.authorized_amount:
            res = [self.total_amount - self.authorized_amount]
            for payment in self.payments:
                res.append(-payment.amount)
        return sum(res)

    def get_authorized_amount(self, name):
        res = []
        if self.copayment:
            for line in self.lines:
                if line.copayment > 0:
                    res.append(line.amount - line.copayment)
        return sum(res)

    # @classmethod
    # @ModelView.button
    # def pay_copayment(cls, services):
    #     for service in services:
    #         if not service.copayment:
    #             continue
    #         service.create_voucher('copayment', service.copayment)

    # def create_voucher(self, kind, amount):
    #     pool = Pool()
    #     Voucher = pool.get('account.voucher')
    #     VoucherConfig = pool.get('account.voucher_configuration')
    #     PayMode = pool.get('account.voucher.paymode')
    #     config = pool.get('health.configuration').get_config()
    #     voucher_config = VoucherConfig.get_configuration()
    #     today = date.today()
    #     payment_mode = None
    #
    #     if voucher_config.default_payment_mode:
    #         payment_mode = voucher_config.default_payment_mode
    #     else:
    #         values = PayMode.search([
    #             ('company', '=', Transaction().context.get('company'))
    #         ])
    #         if values:
    #             payment_mode = values[0]
    #     if not payment_mode:
    #         raise UserError(gettext('missing_paymode'))
    #
    #     if not config.product_copayment:
    #         raise UserError(gettext('missing_product_copayment'))
    #
    #     field_voucher = 'copayment_voucher'
    #     voucher_type = 'receipt'
    #     party = self.patient.party
    #     account_id = Voucher.gcreate_invoice_insuranceet_account(voucher_type, payment_mode)
    #     voucher_to_create = {
    #         'party': party.id,
    #         'voucher_type': voucher_type,
    #         'date': today,
    #         'description': self.number,
    #         'payment_mode': payment_mode.id,
    #         'state': 'draft',
    #         'account': account_id,
    #         'journal': payment_mode.journal.id,
    #         'lines': [('create', [])],
    #         'method_counterpart': 'one_line',
    #     }
    #
    #     account_revenue = config.product_copayment.template.account_category.account_revenue
    #     detail = ('Health Service ' + self.number)
    #     voucher_to_create['lines'][0][1].append({
    #         'detail': detail,
    #         'amount': amount,
    #         'party': self.invoice_to.id,
    #         'amount_original': amount,
    #         'account': account_revenue.id,
    #     })
    #
    #     voucher, = Voucher.create([voucher_to_create])
    #     voucher.on_change_lines()
    #     voucher.save()
    #     Voucher.process([voucher])
    #     self.write([self], {field_voucher: voucher.id})
    #     self.save()

    @classmethod
    def get_invoice_line(cls, line, unit_price, seq):
        # Include taxes related to the product on the invoice line
        taxes = []
        if line.invoice_line:
            return
        for product_tax_line in line.product.customer_taxes_used:
            taxes.append(product_tax_line.id)
        res = {
            'origin': str(line),
            'product': line.product.id,
            'description': line.desc,
            'quantity': line.qty,
            'account': line.product.template.account_revenue_used.id,
            'unit': line.product.default_uom.id,
            'unit_price': unit_price,
            'sequence': seq,
            'taxes': [('add', taxes)],
        }
        return res

    @classmethod
    def get_invoice_line_grouped(cls, service, seq):
        # Include taxes related to the product on the invoice line
        config = Pool().get('health.configuration').get_config()
        product = config.product_ars
        res = {
            'origin': str(service),
            'product': config.product_ars.id,
            'description': service.number,
            'quantity': 1,
            'account': product.account_revenue_used.id,
            'unit': product.default_uom.id,
            'unit_price': service.authorized_amount,
            'copayment': service.copayment,
            'service_start': service.service_date,
            'insurance_plan': service.insurance_plan.id if service.insurance_plan else None,
            'patient': service.patient.id,
            'authorization': service.authorization_code,
            'area': str(service.origin) if service.origin else '',
            'sequence': seq,
            'amount_service': service.total_amount,
            # 'taxes': [('add', taxes)],
        }
        return res

    @classmethod
    def get_invoice_data(cls, party, patient=None, description=''):
        pool = Pool()
        Journal = pool.get('account.journal')
        Party = pool.get('party.party')
        journals = Journal.search([
            ('type', '=', 'revenue'),
            ], limit=1)

        if journals:
            journal, = journals
        else:
            journal = None

        if not party.account_receivable:
            raise UserError(gettext('health_services.msg_no_account_receivable'))

        party_address = Party.address_get(party, type='invoice')
        if not party_address:
            raise UserError(gettext('health_services.msg_no_invoice_address'))

        invoice_data = {
            'party': party.id,
            'type': 'out',
            'journal': journal.id,
            'invoice_date': date.today(),
            'description': description,
            'account': party.account_receivable.id,
            'invoice_address': party_address.id,
            'payment_term': party.customer_payment_term.id,
        }
        if patient:
            invoice_data['patient'] = patient.id
        return invoice_data

    @classmethod
    def create_invoice_insurance(cls, party, services, mode):
        Invoice = Pool().get('account.invoice')
        invoice_data = cls.get_invoice_data(party)
        seq = 0
        invoice_lines = []
        if mode == 'by_lines':
            for service in services:
                for line in service.lines:
                    if line.invoice_line:
                        continue
                    seq += 1
                    if line.to_invoice:
                        unit_price = line.unit_price
                        if line.copayment:
                            unit_price = unit_price - line.copayment
                        _line = cls.get_invoice_line(line, unit_price, seq)
                        if _line:
                            invoice_lines.append(_line)

        else:
            for service in services:
                seq += 1
                _line = cls.get_invoice_line_grouped(service, seq)
                invoice_lines.append(_line)
            invoice_data['lines'] = [('create', invoice_lines)]

        if not invoice_lines:
            return

        invoice_data['lines'] = [('create', invoice_lines)]
        invoices = Invoice.create([invoice_data])
        # for invoice in invoices:
        #     for line in invoice.lines:
        #         line.origin.invoice_line = line.id
        #         line.origin.save()
        return invoices

    def create_services_invoice(self, party, services):
        # There two options for kind: insurance company or patient / party
        pool = Pool()
        Invoice = pool.get('account.invoice')
        MoveLine = pool.get('account.move.line')
        Journal = pool.get('account.journal')
        Service = pool.get('health.service')

        currency_id = Transaction().context.get('currency')
        invoices = []

        journals = Journal.search([
            ('type', '=', 'revenue'),
            ], limit=1)

        if journals:
            journal, = journals
        else:
            journal = None

        # Invoice Header
        for service in services:
            if service.state == 'invoiced':
                continue
            if kind == 'patient' and service.invoice_to:
                party = service.invoice_to
            else:
                party = service.patient.party

            invoice_data = self.get_invoice_data(party, service)
            ctx = {}
            sale_price_list = None
            if hasattr(party, 'sale_price_list'):
                sale_price_list = party.sale_price_list

            if sale_price_list:
                ctx['price_list'] = sale_price_list.id
                ctx['sale_date'] = date.today()
                ctx['currency'] = currency_id
                ctx['customer'] = party.id

            invoice_data['journal'] = journal.id
            party_address = Party.address_get(party, type='invoice')
            if not party_address:
                raise UserError(
                    gettext('health_services.msg_no_invoice_address'))
            invoice_data['invoice_address'] = party_address.id
            invoice_data['reference'] = service.number

            if not party.customer_payment_term:
                raise UserError(gettext('health_services.msg_no_payment_term'))

            # Invoice Lines
            seq = 0
            invoice_lines = []
            for line in service.lines:
                seq = seq + 1

                if sale_price_list:
                    with Transaction().set_context(ctx):
                        unit_price = sale_price_list.compute(party,
                            line.product, line.product.list_price,
                            line.qty, line.product.default_uom)
                else:
                    unit_price = line.product.list_price

                if line.to_invoice:
                    invoice_line = self.get_invoice_line(line, unit_price, seq)
                    invoice_lines.append(('create', [invoice_line]))
                invoice_data['lines'] = invoice_lines

            invoices.append(invoice_data)

        new_invoices = Invoice.create(invoices)
        Invoice.update_taxes(new_invoices)
        invoice = new_invoices[0]
        today = date.today()
        to_reconcile = []
        amount = self.start.payment_amount
        payment_method = self.start.payment_method
        Invoice.post([invoice])
        if self.start.payment_method and invoice.total_amount == amount:
            to_reconcile, remainder = invoice.get_reconcile_lines_for_amount(
                amount)
            to_reconcile = list(to_reconcile)
            lines = invoice.pay_invoice(amount, payment_method, today)
            to_reconcile += lines
            if to_reconcile:
                MoveLine.reconcile(to_reconcile)

        # Change to invoiced the status on the service document.
        Service.write(services, {'state': 'invoiced'})


class HealthServiceLine(metaclass=PoolMeta):
    __name__ = 'health.service.line'
    copayment = fields.Numeric('Copayment', digits=(16, 4))

    @classmethod
    def create(cls, vlist):
        lines = super(HealthServiceLine, cls).create(vlist)
        for line in lines:
            if line.service.insurance_plan:
                price_list = line.service.insurance_plan.price_list
                if price_list:
                    for value in vlist:
                        line.copayment = cls.get_amount_copayment(
                            value['product'],
                            value['unit_price'],
                            price_list)
                        line.save()
        return lines

    @classmethod
    def get_amount_copayment(cls, product_id, unit_price, price_list_id):
        PriceListLine = Pool().get('product.price_list.line')
        lines = PriceListLine.search([
            ('price_list', '=', price_list_id),
            ('product', '=', product_id)
        ])
        amount = ZERO
        if lines:
            try:
                unit_price = float(unit_price)
                amount = Decimal(eval(lines[0].copayment_formula))
            except:
                pass
        return amount

    @fields.depends('product', 'copayment', 'service', 'desc')
    def on_change_product(self):
        super(HealthServiceLine, self).on_change_product()
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        if not self.product:
            return

        if self.service and self.service.insurance_plan:
            price_list = self.service.insurance_plan.price_list
            ctx = {
                'price_list': price_list.id,
                'currency': company.currency.id,
            }

            with Transaction().set_context(ctx):
                unit_price = self.product.template.list_price
                self.copayment = self.get_amount_copayment(
                    self.product.id, unit_price, price_list.id)


class CreateServiceInvoice(metaclass=PoolMeta):
    __name__ = 'health.service.invoice.create'

    def get_invoice_line(self, line, unit_price, seq, type_=None):
        res = super().get_invoice_line(
            line, unit_price, seq)
        if res:
            if type_ == 'ars':
                res['unit_price'] = res['unit_price'] - line.copayment
            else:
                res['unit_price'] = line.copayment
        return res

    def get_invoice_data(self, party, service):
        res = super(CreateServiceInvoice, self).get_invoice_data(party, service)
        if service.insurance_plan:
            res['insurance_plan'] = service.insurance_plan.id
        return res

    def transition_create_invoice(self):
        pool = Pool()
        config = pool.get('health.configuration').get_config()
        if config.invoice_service_method == 'unique':
            super(CreateServiceInvoice, self).transition_create_invoice()
            return

        HealthService = pool.get('health.service')
        Invoice = pool.get('account.invoice')
        Party = pool.get('party.party')
        Journal = pool.get('account.journal')

        currency_id = Transaction().context.get('currency')
        services = HealthService.browse(Transaction().context.get(
            'active_ids'))

        # Invoice Header
        invoices = []
        for service in services:
            if service.state == 'invoiced':
                raise UserError(gettext('duplicate_invoice'))

            for type_ in ['ars', 'consumer']:
                if type_ == 'ars' and service.invoice_to:
                    party = service.invoice_to
                else:
                    party = service.patient.party

                invoice_data = self.get_invoice_data(party, service)
                ctx = {}
                sale_price_list = None
                if hasattr(party, 'sale_price_list'):
                    sale_price_list = party.sale_price_list

                if sale_price_list:
                    ctx['price_list'] = sale_price_list.id
                    ctx['sale_date'] = date.today()
                    ctx['currency'] = currency_id
                    ctx['customer'] = party.id

                journals = Journal.search([
                    ('type', '=', 'revenue'),
                    ], limit=1)

                if journals:
                    journal, = journals
                else:
                    journal = None

                invoice_data['journal'] = journal.id

                party_address = Party.address_get(party, type='invoice')
                if not party_address:
                    self.raise_user_error('no_invoice_address')
                invoice_data['invoice_address'] = party_address.id
                invoice_data['reference'] = service.number
amount_to_pay
                if not party.customer_payment_term:
                    self.raise_user_error('no_payment_term')

                invoice_data['payment_term'] = party.customer_payment_term.id

                # Invoice Lines
                seq = 0
                invoice_lines = []
                for line in service.lines:
                    seq = seq + 1

                    if sale_price_list:
                        with Transaction().set_context(ctx):
                            unit_price = sale_price_list.compute(party,
                                line.product, line.product.list_price,
                                line.qty, line.product.default_uom)
                    else:
                        unit_price = line.product.list_price

                    if line.to_invoice:
                        invoice_line = self.get_invoice_line(
                            line, unit_price, seq, type_)
                        invoice_lines.append(('create', [invoice_line]))

                invoice_data['lines'] = invoice_lines
                invoices.append(invoice_data)

            res = Invoice.create(invoices)
            Invoice.update_taxes(res)
            for inv in res:
                for line in inv.lines:
                    line.origin.invoice_line = line.id
                    line.origin.save()
            # Change to invoiced the status on the service document.
            HealthService.write(services, {'state': 'invoiced'})

        return 'end'


class ServicePaymentForm(metaclass=PoolMeta):
    __name__ = 'health_services.payment_form.start'

    def get_amount_to_pay(self, service):
        amount_to_pay = super().get_amount_to_pay(service)
        return service.balance_payable

    @fields.depends('statement')
    def on_change_with_amount_to_pay(self):
        res = super(ServicePaymentForm, self).on_change_with_amount_to_pay()
        active_id = Transaction().context.get('active_id')
        Service = Pool().get('health.service')
        service = Service(active_id)
        if res and service.customer_balance != 0:
            res = service.customer_balance
        return res


class WizardServicePayment(metaclass=PoolMeta):
    __name__ = 'health_services.payment_form'

    def get_line(self, form, account, service):
        line = super(WizardServicePayment, self).get_line(form, account, service)
        if form.amount_to_pay:
            if service.customer_balance >= form.amount_to_pay:
                line.copayment = True
        return line

    def update_service(self, service):
        super(WizardServicePayment, self).update_service(service)
        if service.copayment > 0:
            if service.customer_balance == 0:
                service.customer_status = 'paid'
            elif service.customer_balance > 0:
                service.customer_status = 'pending'
        else:
            if service.balance_payable == 0:
                service.customer_status = 'paid'
        service.save()


class CreateInsuranceInvoicesStart(ModelView):
    'Create Insurance Invoices Start'
    __name__ = 'health_do.create_insurance_invoices.start'
    start_date = fields.Date('Date Start', required=True)
    end_date = fields.Date('Date End', required=True)
    mode = fields.Selection([
        ('by_lines', 'By Lines'),
        ('by_service', 'By Service'),
        ], 'Mode', required=True)
    insurance_plan = fields.Many2One('health.insurance.plan', 'Insurance Plan',
        required=True)
    institution = fields.Many2One('health.institution', 'Institution',
        required=True)

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()


class CreateInsuranceInvoices(Wizard):
    'Create Insurance Invoices'
    __name__ = 'health_do.create_insurance_invoices'
    start = StateView(
        'health_do.create_insurance_invoices.start',
        'health_do.create_insurance_invoices_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Service = pool.get('health.service')
        party = self.start.insurance_plan.company
        services = Service.search([
            ('service_date', '>=', self.start.start_date),
            ('service_date', '<=', self.start.end_date),
            ('insurance_plan', '=', self.start.insurance_plan.id),
            ('state', 'in', ('invoiced', 'finished', 'paid'))
        ])
        print(' services ...', services)
        Service.create_invoice_insurance(party, services, self.start.mode)
        return 'end'
