# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.wizard import Wizard, StateTransition


class Subdivision(metaclass=PoolMeta):
    __name__ = 'country.subdivision'
    dane_code = fields.Char('Dane Code')


class DaneSubdivision(ModelView, ModelSQL):
    'Dane Subdivision'
    __name__ = 'country.dane_subdivision'
    dane_code = fields.Char('Code Dane', required=True)
    subdivision = fields.Many2One('country.subdivision', 'Subdivision',
            required=True)


class DaneSubdivisionUpdate(Wizard):
    'Dane Subdivision Update'
    __name__ = 'country.subdivision.update'
    start_state = 'update'
    update = StateTransition()

    def transition_update(self):
        DaneSubdivision = Pool().get('country.dane_subdivision')
        dane_subdivisions = DaneSubdivision.search([])
        for dadiv in dane_subdivisions:
            dadiv.subdivision.write([dadiv.subdivision], {
                'dane_code': dadiv.dane_code,
            })
        return 'end'
