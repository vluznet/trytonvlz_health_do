# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool

STATES_PATIENT = {
    'invisible': ~Eval('is_patient')
}


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    type_user = fields.Selection([
        ('1', '1 - Contributivo'),
        ('2', '2 - Subsidiado'),
        ('3', '3 - Privado'),
        ('', ''),
        ], 'Type User', states=STATES_PATIENT)
    type_user_string = type_user.translated('type_user')
    age = fields.Function(fields.Integer('Age'), 'get_age')
    age_uom = fields.Function(fields.Selection([
        ('1', 'Year'),
        ('2', 'Months'),
        ('3', 'Days'),
        ('', ''),
        ], 'UoM Age', states=STATES_PATIENT), 'get_age')
    residential_area = fields.Selection([
        ('U', 'Urbana'),
        ('R', 'Rural'),
        ('', ''),
        ], 'Residential Area', states=STATES_PATIENT)
    limit_invoice_patient = fields.Numeric('Limit Invoice by Patient',
        states={
            'invisible': Bool(Eval('is_person')),
        })

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()

    # @fields.depends('gender', 'sex')
    # def on_change_gender(self, name=None):
    #     if self.gender == 'm':
    #         self.sex = 'male'
    #     else:
    #         self.sex = 'female'

    def get_age(self, name=None):
        if not self.birthday:
            return
        res = datetime.date.today() - self.birthday
        if res.days < 31:
            age = res.days
            age_uom = '3'
        elif res.days < 366:
            age = int(res.days / 30.5)
            age_uom = '2'
        else:
            age = int(res.days / 365.25)
            age_uom = '1'
        if name == 'age':
            return age
        elif name == 'age_uom':
            return age_uom

    @staticmethod
    def default_residential_area():
        return 'U'

    @classmethod
    def default_account_receivable(cls, **pattern):
        pool = Pool()
        Configuration = pool.get('account.configuration')
        config = Configuration(1)
        account_receivable = config.get_multivalue(
            'default_account_receivable', **pattern)
        return account_receivable.id if account_receivable else None

    @classmethod
    def default_account_payable(cls, **pattern):
        pool = Pool()
        Configuration = pool.get('account.configuration')
        config = Configuration(1)
        account_payable = config.get_multivalue(
            'default_account_payable', **pattern)
        return account_payable.id if account_payable else None
