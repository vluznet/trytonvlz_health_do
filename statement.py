# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from datetime import datetime
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.report import Report
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.transaction import Transaction


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'
    copayment = fields.Boolean('Copayment')


class ServiceSquareBoxStart(ModelView):
    'Service Square Box Start'
    __name__ = 'health_do.service_square_box.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    date = fields.Date('Date', required=True)
    device = fields.Many2One('payment.device', 'Device', required=True)
    turn = fields.Selection([
        ('', ''),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
    ], 'Turn')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_user():
        return Transaction().user

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


class ServiceSquareBox(Wizard):
    'Service Square Box'
    __name__ = 'health_do.service_square_box'
    start = StateView(
        'health_do.service_square_box.start',
        'health_do.service_square_box_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('health_do.service_square_box_report')

    def do_print_(self, action):
        report_context = {
            'company': self.start.company.id,
            'date': self.start.date,
            'turn': self.start.turn,
            'device': self.start.device.id,
        }
        return action, report_context

    def transition_print_(self):
        return 'end'


class ServiceSquareBoxReport(Report):
    'Square Box Report'
    __name__ = 'health_do.service_square_box_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        # Device = pool.get('sale.device')
        company = Company(data['company'])
        Statement = pool.get('account.statement')
        User = pool.get('res.user')

        total_amount = []
        total_end_balance = []
        total_start_balance = []
        total_services_cash = []
        count_money = []
        total_money = []
        cash_start_balance = []

        dom_statement = [
            ('date', '=', data['date']),
            ('payment_device', '=', data['device']),
        ]

        if data['turn']:
            dom_statement.append(('turn', '=', int(data['turn'])))

        statements = Statement.search(dom_statement)
        user_id = Transaction().user
        user = User(user_id)

        cashier = None
        for statement in statements:
            kind = statement.journal.kind
            st_amount = sum(l.amount for l in statement.lines)
            total_amount.append(st_amount)
            total_end_balance.append(statement.end_balance)
            total_start_balance.append(statement.start_balance)
            if kind == 'cash':
                total_money.append(statement.total_money or 0)
                count_money.extend(statement.count_money or [])
                total_services_cash.append(st_amount)
                cash_start_balance.append(statement.start_balance)
            else:
                statement_electronic.append(st_amount)

        total_money = sum(total_money)
        total_net_cash = sum(total_services_cash) + sum(cash_start_balance)

        report_context['records'] = statements
        report_context['cashier'] = cashier
        report_context['date'] = data['date']
        report_context['turn'] = data['turn'] or ''
        report_context['company'] = company
        report_context['user'] = user.name
        report_context['print_date'] = datetime.now()
        report_context['total_end_balance'] = sum(total_end_balance)
        report_context['total_start_balance'] = sum(total_start_balance)
        report_context['total_amount'] = sum(total_amount)
        report_context['cash_start_balance'] = sum(cash_start_balance)
        report_context['total_services_cash'] = sum(total_services_cash)
        report_context['total_net_cash'] = total_net_cash
        report_context['total_square'] = total_money - total_net_cash
        report_context['total_money'] = total_money
        report_context['count_money'] = count_money
        # report_context['statement_end_balance'] = end_balance
        return report_context


class CashUpStart(ModelView):
    'Cash Up Start'
    __name__ = 'health_do.cash_up.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    device = fields.Many2One('payment.device', 'Device', required=True)
    user = fields.Many2One('res.user', 'User', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_user():
        return Transaction().user

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


class CashUp(Wizard):
    'Cash Up Box'
    __name__ = 'health_do.cash_up'
    start = StateView(
        'health_do.cash_up.start',
        'health_do.cash_up_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('health_do.cash_up_report')

    def do_print_(self, action):
        report_context = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'user': self.start.user.id,
            'device': self.start.device.id,
        }
        return action, report_context

    def transition_print_(self):
        return 'end'


class CashUpReport(Report):
    'Cash Up Report'
    __name__ = 'health_do.cash_up_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Device = pool.get('payment.device')
        Statement = pool.get('account.statement')
        User = pool.get('res.user')

        total_amount = []

        dom = [
            ('date', '>=', data['start_date']),
            ('date', '<=', data['end_date']),
            ('payment_device', '=', data['device']),
            ('create_uid', '=', data['user']),
        ]

        statements = Statement.search(dom, order=[('date', 'ASC')])

        for statement in statements:
            st_amount = sum(li.amount for li in statement.lines)
            total_amount.append(st_amount)

        report_context['records'] = statements
        report_context['device'] = Device(data['device'])
        report_context['company'] = Company(data['company'])
        report_context['user'] = User(data['user'])
        report_context['total_amount'] = sum(total_amount)
        report_context['print_date'] = datetime.now()
        return report_context
