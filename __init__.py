# This file is part of Tryton. The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import product
from . import invoice
from . import health
from . import health_service
from . import configuration
from . import statement


def register():
    Pool.register(
        configuration.Configuration,
        party.Party,
        health.PatientEvaluation,
        health_service.HealthService,
        health_service.HealthServiceLine,
        health_service.ServicePaymentForm,
        invoice.Invoice,
        invoice.InvoiceLine,
        product.PriceListLine,
        statement.StatementLine,
        statement.ServiceSquareBoxStart,
        statement.CashUpStart,
        health_service.CreateInsuranceInvoicesStart,
        # reports.GeneralEvaluationPatientStart,
        # reports.ProductInvoiceStart,
        module='health_do', type_='model')
    Pool.register(
        health_service.WizardServicePayment,
        health_service.CreateInsuranceInvoices,
        statement.ServiceSquareBox,
        statement.CashUp,
        # reports.GeneralEvaluationPatient,
        # reports.ProductInvoice,
        module='health_do', type_='wizard')
    Pool.register(
        statement.CashUpReport,
        statement.ServiceSquareBoxReport,
        # reports.GeneralEvaluationPatientReport,
        # reports.ProductInvoiceReport,
        module='health_do', type_='report')
